## 1.0.3 (2023-11-16)

### changed (1 change)

- [Include '--fail-with-body' in curl command to halt on server errors.](gitlabci_gallery/release/automate-changelog-generation@2655b50f28ccd4f2726a827b29f7b268df958e4b)

## 1.0.2 (2023-11-16)

### changed (1 change)

- [Fix typo in the README.md](gitlabci_gallery/release/automate-changelog-generation@ea2f809e4466b357f126603693fd7941680c7ce2)

## 1.0.1 (2023-11-16)

### added (1 change)

- [Add automatic `changelog.md` generation via gitlab-ci.yml](gitlabci_gallery/release/automate-changelog-generation@6ac7abe1b9dffd096622dd3657a77fbeda0ef845)

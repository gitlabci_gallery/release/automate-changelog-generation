# Automating Release Notes Generation Workflow for GitLab

Gitlab gives an API to automate the creation of release notes. We present in this tutorial a workflow to automatically update the `changelog.md` file on a release (i.e., CI triggered by a tag).

## GitLab Changelog API

GitLab offers an API that generates project changelogs based on commit titles, Git trailers, and tags. The output follows a structured format: a primary section named after the tag and its corresponding release date, containing commits created since the previous tag. These commits are categorized by the Git trailer `Changelog`. For instance, when using the default template with a dummy project, the API might return:

```
## 1.0.0 (2023-11-14)

### added (2 changes)

- [Add automatic `changelog.md` generation via gitlab-ci.yml (!2)](alpetit/dummy@ba19d109e96f0265c4f9644ef080e63208ad021b)
- [Add helm installation recipe (!1)](alpetit/dummy@fa3d1f663faf078da99d1231d39e7580903e225a)

### changed (1 change)

- [Improve documentation in README.md (!3)](alpetit/dummy@dd29d189e96f02fv55f9644ef080e5c6a4ad066c)
```

Note: GitLab provides the flexibility to customize the generated output by creating a custom template.

To add a trailer to a Git commit, you can either include `Changelog: xxx` after the commit description or use the `--trailer` parameter with the argument `'Changelog: xxx'` in the `git commit` command. Here is an example for the first case:

```
<Commit title>

<Commit description>

Changelog: added
```

There is no convention for changelog trailer values. Every project needs to make its own list of possible categories.

More information on GitLab Changelog API can be found on the [official Gitlab documentation](https://docs.gitlab.com/ee/user/project/changelogs.html).


## Add a step to update 'changelog.md' file

### Create an Access Token

For the creation of an Access Token, you can refer to an existing example in the gallery. Check out the [example provided here](https://gitlab.inria.fr/gitlabci_gallery/gitlab-api/project-statistics/-/blob/main/README.md) for detailed instructions.

Here are specific guidelines:
- Ensure the role aligns with the settings in 'Protected branches'; typically, 'maintainers' should be selected.
- The scope must include at least 'api' and 'write_repository'.
- Choose the Access Token name carefully as it will be the author for commits changing the 'changelog.md' file.


### Add the step in `.gitlab-ci.yml`

```
update_changelog:
  stage: deploy
  tags:
    - ci.inria.fr
    - small
    - linux
  image: alpine:latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/'
  script:
    - apk add curl
    - 'curl -H "PRIVATE-TOKEN: ${PROJECT_TOKEN}" --fail-with-body --data "message=Add changelog for version $CI_COMMIT_TAG [ci skip]" -X POST "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/changelog?version=$CI_COMMIT_TAG"'
```

Note:
- `PROJECT_TOKEN` represents the variable name used to store your Access Token. Adapt it if you opt for a different variable name.
- the function of `--data "message=Add changelog for version $CI_COMMIT_TAG [ci skip]"` is to append `[skip ci]` to the default commit message, preventing the CI from running again when this new commit is pushed.
- you can find descriptions of all available attributes that can be used with `--data` in the [Gitlab documentation](https://docs.gitlab.com/ee/api/repositories.html#supported-attributes).
- `if: '$CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/'` is a rule to restrict the execution of the step on tag following a specific pattern, change it to match your tag pattern if needed.


### Result

Executing this step will create a commit that adds the release notes to the 'changelog.md' file. The author of the commit is the name of the Access Token you created.


